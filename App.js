import React from "react";
import {
    Container,
    Header,
    Content,
    Footer,
    FooterTab,
    Button,
    Icon,
    Card,
    CardItem,
    Body,
    Text
} from "native-base";

export default class App extends React.Component {
    async componentWillMount() {
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
        });
    }

    render() {
        return (
            <Container>
                <Header/>
                <Content>
                    <Card>
                        <CardItem>
                            <Body>
                            <Text>//Your text here</Text>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button vertical>
                            <Icon name="apps"/>
                            <Text>Apps</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="camera"/>
                            <Text>Camera</Text>
                        </Button>
                        <Button vertical active>
                            <Icon active name="navigate"/>
                            <Text>Navigate</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="person"/>
                            <Text>Contact</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
