import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";

export default class MainArea extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}> MainArea</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: "white"
  }
});
